<?php


namespace App\Services;


use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebinarisServices
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var HttpClientInterface
     * @author Pradeep
     */
    private $http_client;
    /**
     * @var string
     * @author Pradeep
     */
    private $webinaris_password;
    /**
     * @var string
     * @author Pradeep
     */
    private $webinaris_id;
    private $webinaris_api;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->http_client = HttpClient::create();
        $this->webinaris_api = $this->getWebinarisAPIURL();
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getWebinarisAPIURL(): string
    {
        $webinaris_api = '';
        if (isset($_ENV[ 'WEBINARIS_API' ]) && filter_var($_ENV[ 'WEBINARIS_API' ], FILTER_VALIDATE_URL)) {
            $webinaris_api = $_ENV[ 'WEBINARIS_API' ];
        }
        return $webinaris_api;
    }

    /**
     * @param string $webinaris_password
     * @param string $webinaris_id
     * @return bool
     * @author Pradeep
     */
    public function setWebinarisAccount(string $webinaris_password, string $webinaris_id): bool
    {
        $status = false;
        if (!empty($webinaris_password) && !empty($webinaris_id)) {
            $this->webinaris_password = $webinaris_password;
            $this->webinaris_id = $webinaris_id;
            $status = true;
        }
        return $status;
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getWebinarisDates(): array
    {
        $webinaris_dates = [];
        $webinaris_id = $this->getWebinarisId();
        $webinaris_password = $this->getWebinarisPassword();
        if (empty($webinaris_id) || empty($webinaris_password)) {
            $this->logger->error('Invalid Id ' . $webinaris_id . ' or Password ' . $webinaris_password,
                [__METHOD__, __LINE__]);
            return $webinaris_dates;
        }
        $url = $this->webinaris_api . 'showtimes?webinaris_id=' . $webinaris_id . '&api_password=' . $webinaris_password;
        try {
            $response = $this->http_client->request('GET', $url);
            if ($response->getStatusCode() !== 200) {
                $this->logger->error('Failed to fetch webinaris dates info', [__METHOD__, __LINE__]);
                return $webinaris_dates;
            }
            $content = $response->toArray();
            if (isset($content[ 'status' ], $content[ 'data' ]) && $content[ 'status' ] = 'success') {
                $webinaris_dates = $content[ 'data' ];
            }

        } catch (TransportExceptionInterface | ClientExceptionInterface |
        RedirectionExceptionInterface | ServerExceptionInterface | DecodingExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $webinaris_dates;
    }

    /**
     * @param array $participant
     * @return bool
     * @author Pradeep
     */
    public function createParticipant(array $participant) : bool
    {
        $status = false;
        $webinaris_id = $this->getWebinarisId();
        $webinaris_password = $this->getWebinarisPassword();
        if(empty($participant) || empty($webinaris_id) || empty($webinaris_password) || empty($this->webinaris_api)) {
            $this->logger->error('invalid input', [__METHOD__, __LINE__]);
            return $status;
        }
        if(!$this->checkForMandatoryFields($participant)) {
            $this->logger->error('invalid participant details', [__METHOD__, __LINE__]);
            return $status;
        }
        $time = empty($participant[ 'time' ]) ? urlencode(''): urlencode($participant[ 'time' ]);
        $ip = $participant[ 'ip' ] ?? '';
        $paid = $participant[ 'paid' ] ?? '';
        $url = $this->webinaris_api . '?key=' . $webinaris_id . '&is_paid=' . $paid . '&time=' . urlencode($time) .
               '&email=' . $participant['email'] .'&firstname=' . $participant['firstname'] .
               '&lastname=' . $participant['lastname'] . '&ip_address=' . $ip . '&password=' . $webinaris_password;
        if(!filter_var($url, FILTER_VALIDATE_URL)) {
            $this->logger->error('Invalid URL', [__METHOD__, __LINE__]);
            return $status;
        }
        $this->logger->info($url);
        try {
            $response = $this->http_client->request('GET', $url);
            if ($response->getStatusCode() === 200) {
                $this->logger->error('Failed HTTP request', [__METHOD__, __LINE__]);
                $status = $response->toArray();
            }
        } catch (
                TransportExceptionInterface | ClientExceptionInterface |
                RedirectionExceptionInterface | ServerExceptionInterface |
                DecodingExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param array $participant
     * @return bool
     * @author Pradeep
     */
    private function checkForMandatoryFields( array $participant) : bool
    {
        $status = false;
        if(!empty($participant) && isset($participant['email'], $participant['firstname'], $participant['lastname'])) {
            $status = true;
        }

        return $status;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getWebinarisId(): string
    {
        $webinaris_id = '';
        if ($this->webinaris_id !== null) {
            $webinaris_id = $this->webinaris_id;
        }
        return $webinaris_id;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getWebinarisPassword(): string
    {
        $webinaris_password = '';
        if ($this->webinaris_password !== null) {
            $webinaris_password = $this->webinaris_password;
        }
        return $webinaris_password;
    }

    /**
     * @param string $encoded_string
     * @return array
     * @author Pradeep
     */
    public function validateRequest(string $encoded_string): array
    {
        $req_arr = [];
        if (empty($encoded_string)) {
            $this->logger->error('Invalid String', [__METHOD__, __LINE__]);
            return $req_arr;
        }
        $req_json = base64_decode($encoded_string);
        $req_arr = json_decode($req_json, true, 512, JSON_UNESCAPED_SLASHES);
        return $req_arr;
    }
}