<?php


namespace App\Controller;


use App\Services\WebinarisServices;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class WebinarisController extends AbstractController
{

    /**
     * Route No 1
     * @Route("/getWebinarisDates",name="getWebinarisDates",methods={"POST"})
     * @param Request $request
     * @param WebinarisServices $webinarisServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function getWebinarisDates(
        Request $request,
        WebinarisServices $webinarisServices,
        LoggerInterface $logger
    ): Response {
        $response = new Response();
        $content = $request->getContent();
        $webinaris_params = $webinarisServices->validateRequest($content);
        $logger->info(json_encode($webinaris_params));
        if (!isset($webinaris_params[ 'apiPassword' ], $webinaris_params[ 'webinarId' ]) ||
            !$webinarisServices->setWebinarisAccount($webinaris_params[ 'apiPassword' ],
                $webinaris_params[ 'webinarId' ])) {
            return $response->setContent(json_encode(['error' => 'Invalid APIKey']));
        }
        $webinaris_dates = $webinarisServices->getWebinarisDates();
        return $response->setContent(json_encode($webinaris_dates));
    }

    /**
     * Route No 1
     * @Route("/create_participant",name="create_participant",methods={"POST"})
     * @param Request $request
     * @param WebinarisServices $webinarisServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function createParticipant(
        Request $request,
        WebinarisServices $webinarisServices,
        LoggerInterface $logger
    ): Response {
        $response = new Response();
        $content = $request->getContent();
        $webinaris_params = $webinarisServices->validateRequest($content);
        if (!isset($webinaris_params[ 'apiPassword' ], $webinaris_params[ 'webinarId' ]) ||
            empty($webinaris_params[ 'participant' ])) {
            return $response->setContent(json_encode(['error' => 'Invalid APIKey']));
        }

        if(!$webinarisServices->setWebinarisAccount($webinaris_params[ 'apiPassword' ],
                                                    $webinaris_params[ 'webinarId' ])) {
            return $response->setContent(json_encode(['error' => 'Invalid APIKey']));
        }
        $status = $webinarisServices->createParticipant($webinaris_params[ 'participant' ]);

        return $response->setContent(json_encode($status));
    }

    /**
     * @Route("/check/alive", name="checkalive", methods={"GET"})
     */
    public function checkAlive(Request $request): JsonResponse
    {
        return $this->json([
            'your ip' => $request->getClientIp(),
            'alive' => true,
        ]);
    }

}
